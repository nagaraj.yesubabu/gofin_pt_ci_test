#!/bin/bash

JMETER_VERSION="5.1.1"

# Example build line
# --build-arg IMAGE_TIMEZONE="india/kolkata"
docker build  --build-arg JMETER_VERSION=${JMETER_VERSION} -t "samurai/jmeter:${JMETER_VERSION}" .
